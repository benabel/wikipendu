/**
 * Module gérant les requêtes sur wikipedia pour le choix des mots
 *
 * MediaWiki API Demos
 * Demo of `Parse` module: Parse content of a page
 *
 * MIT License
 */

/**
 * Vérifie si le mot est constitué uniquement de caractères alphabétiques ascii
 * ou espace et/ou tiret (2 max en tout)
 *
 * @param {string} mot
 * @returns boolean
 */
function isValid(mot) {
  // Pas plus de 2  espace/tiret
  if (mot.split(/-|\s/g).length > 3) {
    return false;
  }
  // Convertit la chaine de caractère en liste de lettres majuscules
  const lettres = mot.toUpperCase().split("");
  // Vérifie qu'il s'agit de lettres majuscules ASCII entre 65(A) et 90(Z)
  // ou espace(32) ou tiret(45)
  // let charCodes = [...Array(26).keys()].map(e=> e+65);
  return lettres.every(
    (lettre) =>
      (65 <= lettre.charCodeAt(0) && lettre.charCodeAt(0) <= 90) ||
      lettre.charCodeAt(0) === 32 ||
      lettre.charCodeAt(0) === 45
  );
}

/**
 * Fonction asynchrone pour la récupération de données wikipedia
 * en utilisant l'api REST
 * https://fr.wikipedia.org/api/rest_v1/#/Page%20content/get_page_random__format_
 *
 * @returns
 */
async function getRandomWikipediaArticle() {
  let response = await fetch(
    "//fr.wikipedia.org/api/rest_v1/page/random/summary",
    {
      headers: {
        Accept: "application/problem+json",
      },
      method: "GET",
    }
  );
  try {
    // you can check for response.ok here, and literally just throw an error if you want
    return await response.json();
  } catch (e) {
    console.error("Fetch error!", e);
  }
}

export const fetchContent = async () => {
  // On choisit un article wikipedia au hasard à découvrir
  let data = await getRandomWikipediaArticle();
  while (!isValid(data.title)) {
    console.log("Mot non conforme", data.title);
    data = await getRandomWikipediaArticle();
  }
  return data;
};
