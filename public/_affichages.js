import { initPartie, tourJeu, initJeu } from "./index.js";
import { isBestScore } from "./_utils.js";

/**
 * affiche les lettres du mot trouvé
 * @param array du mot
 * @returns None
 */
export function afficheMot(mot) {
  mot = mot.map((mot) => `<kbd>${mot}</kbd>`);
  document.querySelector("#mot-a-trouver").innerHTML = mot.join(
    "<kbd>&nbsp;</kbd>"
  );
}

/**
 * affiche le nb d'erreurs
 *
 * @param {Number} nbErreurs
 */
export function afficheErreurs(nbErreurs) {
  document.querySelector("#erreurs").innerHTML = nbErreurs;
  document.querySelector("#img-pendu").src = `imgs/Hangman-${nbErreurs}.png`;
}

export function afficheBestScore() {
  const bestScore = Number(window.localStorage.getItem("bestScore"));
  document.querySelector("#best-score").innerHTML = Math.round(bestScore);
}

/**
 * Affiche les aides(image, description) issues de wikipedia
 *
 * @param {object} data objet contenant les données WP
 */
export function afficheAides(data) {
  const helpDiv = document.querySelector("#help");
  // On vide avant
  helpDiv.innerHTML = "";

  // image placée à droite en flottant
  const img = document.createElement("img");
  img.src = data.thumbnail.source;
  img.className = "right";
  img.alt = `Image représentative de ${data.description}`;

  helpDiv.appendChild(img);

  // description
  const description = document.createElement("p");
  description.innerHTML = `<strong>Description:</strong> ${data.description}.`;

  helpDiv.appendChild(description);

  // Extrait de l'article nettoyé du mot à chercher
  const extract = document.createElement("p");
  // Nettoyage de l'extrait en remplacant le mot cherché par des ?
  const regexp = RegExp(data.title, "ig");
  const matches = data.extract.matchAll(regexp);
  // Les string étant immutables on passe par une array
  let extractLetters = data.extract.split("");
  for (const match of matches) {
    for (
      let index = match.index;
      index < match.index + match[0].length;
      index++
    ) {
      extractLetters[index] = "?";
    }
  }
  // Maintenant on recrée l'extrait à partir de l'array modifiée
  extract.innerHTML = `<strong>Extrait de l'article:</strong> ${extractLetters.join(
    ""
  )}.`;

  helpDiv.appendChild(extract);
}

/**
 * Cree et retourne un élément HTML de type kbd contenant la lettre souhaitée
 *
 * L'élément HTML lettre a un écouteur d'événements pour lancer la fonction jeu
 * à chaque click sur une nouvelle lettre
 *
 * Chaque bouton contient également des données pour savoir si:
 * - la lettre a déjà été sélectionné,
 * - si elle est dans le mot
 *
 * @param string du texte
 * @returns {ChildNode} HTML element
 */
function letterToHTMLKbd(letter, motCache) {
  let template = document.createElement("template");
  let html = "<kbd>" + letter.trim() + "</kbd>"; // Never return a text node of whitespace as the result
  template.innerHTML = html;
  let kbd = template.content.firstChild;
  kbd.setAttribute("id", "id-" + letter);
  kbd.setAttribute("class", "letter");
  kbd.addEventListener(
    "click",
    (elt) => {
      // Recarde la fenêtre sur le jeu
      const game = document.querySelector("#game");
      game.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "center",
      });
      // si déjà cliqué return
      if (kbd.classList.contains("selected")) {
        return;
      }
      if (motCache.includes(letter)) {
        // classe pour css
        kbd.setAttribute("class", "letter selected guessed");
      } else {
        // classe pour css
        kbd.setAttribute("class", "letter selected missed");
      }
      // lance le tour de jeu
      tourJeu(letter, motCache);
    },
    this,
    { once: true }
  );

  return kbd;
}

/**
 * Ajout des lettres de l'aphabet à cliquer
 *
 * @export
 */
export function afficheAlphabet(motCache) {
  const alphabet = document.querySelector("#alphabet");
  // on commence par nettoyer si on recommence
  alphabet.innerHTML = "";
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("").forEach(function (l) {
    alphabet.appendChild(letterToHTMLKbd(l, motCache));
  });
}

/**
 * Affiche un popup
 *
 * @param Objet configuration du modal clés titre et texte
 *
 * @export
 */
export function afficheModal(conf) {
  const { titre, texte } = conf;
  // Affiche textes
  document.getElementById("modal-titre").innerHTML = titre;
  document.getElementById("modal-text").innerHTML = texte;
  // When the user clicks on the button, open the modal
  // Get the modal
  const modal = document.getElementById("myModal");
  modal.style.display = "block";

  // Ajout du texte si le meileur score est battu
  if (isBestScore()) {
    document.getElementById("modal-footer").innerHTML =
      "BRAVO. Nouveau Record.";
  }
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];
  if (titre === "PERDU") {
    span.style.display = "none";
    const button = document.querySelector("#modal-button");
    button.style.display = "block";
    button.innerHTML = "Nouvelle partie";
    button.onclick = () => {
      document.getElementById("myModal").style.display = "none";
      initPartie();
    };
  } else if (titre === "GAGNÉ") {
    span.style.display = "inline";
    const button = document.querySelector("#modal-button");
    button.style.display = "none";
    initJeu();
    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
      modal.style.display = "none";
    };
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    };
  }
}
