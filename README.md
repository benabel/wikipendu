# WikiPendu

> Jouez au pendu en utilisant les informations données par l'encyclopédie libre Wikipedia.
> Projet réalisé dans le cadre du DIU informatique à l'université de Nice Côte d'Azur.

Une version de démonstartaion est disponible à l'adresse suivante: https://benabel.frama.io/wikipendu/

## Introduction

La particularité de notre pendu est que **les mots à deviner sont issus de l'[API
Rest de Wikipédia](https://fr.wikipedia.org/api/rest_v1/)**.

À chaque nouveau jeu, une requête `GET` est envoyée vers l’endpoint*
[`page/random/summary`](https://fr.wikipedia.org/api/rest_v1/#/Page%20content/get_page_random__format*).
On récupère alors des données au format `json` à partir desquelles on va créer
des indices pour le mot à deviner:

1. **Le mot à deviner**: limité uniquement à des caractères ASCII avec
   éventuellement un espace ou un tiret avec une limite de 2 signes de
   ponctuation. pour plus de détails voir la fonction `isValid` du module
   `_fetch-wikipedia.js`.
2. **Une vignette**: utilisant l'url du _thumbnail_ envoyée par l'API.
3. **Un extrait de l'article** dans lequel le mot à chercher est remplacé par
   des `?` grâce à une expression régulière. Pour plus de détails, consulter la
   fonction `afficheAides()` du module `_affichages.js`.

On obtient alors une section de ce type dans le document:

![Copie d'écran de la section indices](./ce-indices.png)

Autre particularité, ce jeu permet de **deviner plusieurs mots** au cours d'une
partie, et ainsi d'augmenter son score dans le but de battre le meilleur score
enregistré dans la mémoire locale du navigateur.

## Organisation du code et gestion de l'état du programme

Le parti pris du développement est de ne pas soucier de la compatibilité et
d'utiliser toutes les dernières fonctionnalités des navigateurs. **Aucune
bibliothèque extérieure n'est utilisée.**

Le développement est effectué sur _firefox_, cependant les quelques essais
effectués sur _chromium_ semblent indiquer que le jeu y est fonctionnel
également.

### Utilisation des modules `js`

Le module principal du jeu est importé en tant que module `<script type="module" src="index.js"></script>`, ainsi il a été possible de séparer les diverses
fonctionnalités du jeu en divers module.

### Utilisation d'une fonction asynchrone pour la récupération des données

Le démarrage du jeu nécessitant l'attente de la réponse de l'API, une fonction
asynchrone a été créé afin de conditionner le démarrage du jeu à la réponse du
serveur.

```js
async function getRandomWikipediaArticle() {
  let response = await fetch(
 ...)
 ...
 return await response.json();
  }
```

Cette fonction du module `_fetch-wikipedia.js` est ensuite utilisée lors de
l'initialisation du jeu avec la méthode `then()` qui attend la résolution de la
`Promise` `fetch` avant l'initialisation du jeu.

```js
export function initJeu() {
  ...
  // Récupération des données asynchrone
  fetchContent().then((data) => {
    ...}
  ...
}
```

### Approche fonctionnelle de la programmation

Le javascript étant un programme possédant de nombreuses influences des
paradigmes de programmation fonctionnelle, ce programme n'utilise que très peu
les boucles, mais tire partie des méthodes puissantes sur le `Array` et ou
`String`:

- `.forEach()`
- `.map()`
- `.includes()`
- affectation par décomposition (destructuring en anglais).

Par exemple dans la fonction `getLettresTrouvées` du module `utils.js`:

```js
function getLettresTrouvées(motCache) {
  // on récupère les lettres trouvées à partir des éléments html
  const guessedEls = document.querySelectorAll(".guessed");
  let guessedLetters = [...guessedEls].map((node) => node.innerHTML);

  // Les espaces et tirets ne sont pas à deviner!
  guessedLetters = guessedLetters.concat([" ", "-"]);

  // On retourne la liste
  const lettresTrouvées = motCache
    .split("")
    .map((l) => (guessedLetters.includes(l) ? l : "_"));

  return lettresTrouvées;
}
```

### Gestion de l'état de l'application

La gestion du meilleur score est effectuée via l'API `localstorage`, ainsi le
meilleur score peut perdurer entre diverses ouvertures du navigateur. Voici le
code d'initialisation.

```js
function initStorage() {
  if (!window.localStorage.getItem("bestScore")) {
    window.localStorage.setItem("bestScore", 0);
  }
}
```

Le mot à trouver est passé par arguments à toutes les fonctions qui l'utilisent,
ainsi il reste caché pour l'utilisateur(même celui sachant utiliser une console.
J'ai laissé le `console.debug()` pour savoir ce qu'on cherche pendant les
tests.)

Par contre, le reste des données d'état de l'application est stocké dans les
`data-attributes` des éléments `html` de la page.

- Le score est stocké dans l'attribut `data-score` de l'élément `#score`.
- Les lettres de l'alphabet interactif contiennent des informations sous forme
  de classes, permettant ainsi de modifier facilement leur rendu en `css`:
  - Le fait que la lettre ait déjà été sélectionnée est stockée dans la classe
    `.selected`.
  - Les lettres fausses et les justes sont stockées sous forme des classes
    `.missed` et `.guessed`.

_Remarque: cette gestion des données bien que pratique, possède cependant un
inconvénient important, les données ne sont pas désolidarisées du contenu, et il
faut bien faire attention à quel moment on appelle la fonction(avant le choix de
la lettre ou après, avant l'initialisation de l'alphabet ou après...)._

### Structure du programme: ordre d'exécution des fonctions

À l'ouverture, le programme commence par initialiser le `localstorage` pour
connaitre le meilleur score, puis va lancer une partie grâce à la fonction
`initPartie()` qui a pour but d'initialiser le score du jeu à 0.

`initPartie()` lance alors la fonction `initJeu()` qui choisit un article de
Wikipédia via son API REST en restreignant quelques peu le choix de l'article à
deviner(cf. l'introduction).

Une fois le mot choisi, `initJeu()` initialise les affichages des scores, des aides
et créé l'_alphabet interactif_.

L'_alphabet interactif_ est constitué des 26 lettres de l'alphabet. Sur chacune
d'elle est attachée un écouteur d'événement sur le `click` qui lance la fonction
`tourJeu` avec en argument la lettre choisie et le mot caché qui mettra à jour
l'état du jeu en fonction des arguments:

- Si la lettre fait partie du mot caché, on augmente le score puis on vérifie si
  le joueur a trouvé le mot. Dans ce cas la fonction `initJeu()` est lancée pour
  proposer un autre mot et pouvoir ainsi augmenter son score.
- Sinon, on vérifie si le joueur a perdu, c'est-à-dire s'il a fait plus de 6
  erreurs, puis on relance une partie avec la fonction `initPartie()`.

## Utilisation du jeu: Audience, UI, UX.

### Audience

Ce jeu a pour principal intérêt de toujours proposer des nouveaux mots par le
biais de l'**aspect aléatoire** du _endpoint_ de l'API. Cependant les mots à
chercher peuvent s'avérer _assez difficiles_ à trouver car certains articles
traitent de sujets pour le moins _confidentiels_! Il semble plutôt approprié
pour des adultes ou personnes ayant une grande culture générale.

Ce jeu n'a pas été étudié ni essayé sur un téléphone ou une tablette. Cependant,
le clavier interactif devrait permettre d'y jouer sur des écrans tactiles, et la
plupart de la mise en page utilisant des éléments positionnés dans le flux
devraient permettre d'accéder à l'ensemble des éléments de la page pour des
écrans de taille raisonnable.

### Interface utilisateur

L'interface se veut minimaliste, et _"copie"_ le style tout en sobriété de Wikipédia.

L'interface est séparée en trois parties:

1. Comment jouer?:
2. Jeu
3. Indices(cf. Introduction)
4. Compteurs

### Expérience utilisateur

L'alphabet interactif permet de jouer facilement par simples clics de souris, et
en même temps de contrôler visuellement les lettres justes et fausses. Il est
accompagné d'images du pendu issues de Wikipédia pour compter le nombre
d'erreurs, ainsi que de l'écriture du mot traditionnelle avec les lettres
trouvées et les lettres à trouver sous forme de tirets bas.

Il est également possible de jouer avec le clavier, pour cela les événements de
pression de touches sont tout simplement répercutés sous la forme d'un clic sur
la lettre.

```js
// Ajout d'un écouteur d'événements sur le touches alpha du clavier pour jouer
document.addEventListener("keypress", (e) => {
  if ("abcdefghijklmnopqrstuvwxyz".includes(e.key)) {
    // console.debug(`Touche ${e.key.toUpperCase()} pressée`);
    document.querySelector(`#id-${e.key.toUpperCase()}`).click();
  }
});
```

D'autres part une animation `css` a été ajoutée pour bien faire apparaitre la sélection d'une touche.

```css
/* Animation sur la sélection */
@keyframes letter-selection {
  25% {
    background-color: #333;
    color: var(--wp-light-blue);
    font-weight: bolder;
    margin: 0em;
    padding: 0.8em;
  }
}

.selected {
  animation-name: letter-selection;
  animation-duration: 0.7s;
  animation-timing-function: ease-in-out;
}
```

Enfin, à chaque touche pressée ou sélectionné, le jeu est recentré pour que
l'affichage reste centré sur l'interface du jeu et ne pas être géné par d'autres
éléments de la page tels que les explications d'introduction ou le pied de page.

## Conclusion

Ce projet nous a permis d'améliorer notre connaissance des langages de
programmation du web côté client. La rapide évolution des technologies du web
nécessite une _veille technologique quasi permanente_, les navigateurs ne
cessent de s'améliorer et d'implémenter de nouvelles fonctionnalité au fur et à
mesure des versions publiées. La _standardisation de ces langages_ permet de
créer des _applications web interactives de grande qualité pouvant être
accessibles à toute personne_ ayant accès au réseau internet sans aucune
installation.
