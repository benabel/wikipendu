/* 
Module contenant les divers fonctions utilitaires "getters setters", compteurs...

*/

/**
 * Renvoie le nombre d'erreurs basées sur les classes des lettres de l'alphabet
 * interactif
 *
 * @export
 * @returns Number
 */
export function getNbErreurs() {
  return document.querySelectorAll(".missed").length;
}

//
// Score
//
/**
 * Renvoie le score tel que stocké dans l'élément #score
 *
 * On utilise un data attribute car le score affiché est arrondi
 * Alors que le score réél calculé avec des probabilités est un flottant
 *
 * @export
 * @returns Number
 */
export function getScore() {
  const scoreEl = document.querySelector("#score");
  const score = scoreEl.getAttribute("data-score");
  return Number(score);
}

/**
 * MàJ le score stocké et affiché dans l'élément #score
 *
 * On utilise un data attribute car le score affiché est arrondi
 * Alors que le score réél calculé avec des probabilités est un flottant
 *
 * @export
 */
export function setScore(score) {
  const scoreEl = document.querySelector("#score");
  scoreEl.setAttribute("data-score", score);
  scoreEl.innerHTML = Math.round(score);
}

/**
 * Renvoie la liste des lettres trouvées placées correctement dans le mot caché
 *
 * Les lettres non trouvées sont remplacées par `_`
 *
 * Exemple: ["T","O","_","A","T","_","A"]
 * pour le mot caché TOMIATA avec pour lettres trouvées T, O et A
 *
 * @export
 * @param {str} motCache le mot caché
 * @returns Array
 */
export function getLettresTrouvées(motCache) {
  // on récupère les lettres trouvées à partir des éléments html
  const guessedEls = document.querySelectorAll(".guessed");
  let guessedLetters = [...guessedEls].map((node) => node.innerHTML);

  // Les espaces et tirets ne sont pas à deviner!
  guessedLetters = guessedLetters.concat([" ", "-"]);

  // On retourne la liste
  const lettresTrouvées = motCache
    .split("")
    .map((l) => (guessedLetters.includes(l) ? l : "_"));

  return lettresTrouvées;
}

/**
 * Valeur d'une lettre = Inverse de la probabilité de la trouver
 *  nb lettres non choisies / nb justes restantes
 *
 * @export
 */
export function calculScoreLettre(motCache) {
  const nonChoisies = 26 - document.querySelectorAll(".selected").length;

  // Trouve le nb de lettres trouvées en utilisant un Set
  let setJustes = new Set(motCache.split(""));
  setJustes.delete("_");
  // On ajoute 1 car au moment ou ontrouve une lettre elle n'est pas encore "guessed"
  const nbJustes =
    setJustes.size - document.querySelectorAll(".guessed").length + 1;

  return nonChoisies / nbJustes;
}

/**
 * Vérifie si le score est le nouveau meilleur
 *
 * Si oui Met à jour le localStorage, et renvoie true
 * Sinon renvoie false
 *
 * @export
 * @returns boolean
 */
export function isBestScore() {
  const score = getScore();
  const bestScore = Number(window.localStorage.getItem("bestScore"));
  if (score > bestScore) {
    window.localStorage.setItem("bestScore", score);
    return true;
  } else {
    return false;
  }
}
