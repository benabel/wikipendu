/* 
Script principal du jeu à partir duquel sont importés 
les fonctions des modules annexes pour plus de lisibilité
*/
import { fetchContent } from "./_fetch-wikipedia.js";

import {
  afficheErreurs,
  afficheAides,
  afficheBestScore,
  afficheMot,
  afficheAlphabet,
  afficheModal,
} from "./_affichages.js";

import {
  getNbErreurs,
  calculScoreLettre,
  setScore,
  getScore,
  getLettresTrouvées,
} from "./_utils.js";

/**
 * Initialisation du local storage
 *
 * Il est utilisé pour stocker le high score.
 *
 */
function initStorage() {
  if (!window.localStorage.getItem("bestScore")) {
    window.localStorage.setItem("bestScore", 0);
  }
}

/**
 * Lance un tour de jeu avec la lettre proposée
 *
 * @param {string} letter
 * @param {string} motCache
 */
export function tourJeu(letter, motCache) {
  let score = getScore();

  if (motCache.includes(letter)) {
    // Ajout du score de la lettre
    // C'est un flottant mais il est arrondi dans l'affichage
    score += calculScoreLettre(motCache);
    setScore(score);

    // MàJ affichage du mot
    const lettresTrouvées = getLettresTrouvées(motCache);
    afficheMot(lettresTrouvées);

    // GAGNE
    if (!lettresTrouvées.includes("_")) {
      afficheModal({
        titre: "GAGNÉ",
        texte: `Votre score est de ${Math.round(getScore())}`,
      });
      return;
    }
  } else {
    const nbErreurs = getNbErreurs();
    afficheErreurs(nbErreurs);
    // PERDU
    if (nbErreurs >= 6) {
      afficheModal({
        titre: "PERDU",
        texte: `Le mot caché était: ${motCache}. Votre score est de ${Math.round(
          getScore()
        )}`,
      });
      return;
    }
  }
}

/**
 * Fonction d'initialisation de la partie entière
 *
 * @export
 */
export function initPartie() {
  // on initilise le score à 0
  setScore(0);
  initJeu();
}

/**
 * Fonction d'initialisation du jeu sur un mot
 *
 * @export
 */
export function initJeu() {
  // MàJ affichages
  afficheErreurs(0);
  afficheBestScore();

  // On vide et on remplace par le spinner le temps du chargement
  document.querySelectorAll("#game > .cadre").forEach((e) => {
    e.style.visibility = "hidden";
  });
  const helpDiv = document.querySelector("#help");
  helpDiv.innerHTML = "";
  helpDiv.className = "center loader";
  const gameDiv = document.querySelector("#game");
  gameDiv.className = "center loader";

  // Récupération des données asynchrone
  fetchContent().then((data) => {
    // On enlève le loader et on remet les éléments visbles
    helpDiv.className = "";
    gameDiv.className = "";
    document.querySelectorAll("#game > .cadre").forEach((e) => {
      e.style.visibility = "visible";
    });

    const motCache = data.title.toUpperCase();
    console.debug(`DEBUG Le mot à trouver est: ${motCache}`);
    console.debug(data);

    // MàJ affichages
    afficheAlphabet(motCache);
    afficheAides(data);
    // à afficher après l'alphabet car il se base dessus
    const lettresTrouvées = getLettresTrouvées(motCache);
    afficheMot(lettresTrouvées);
  });
}

window.onload = function () {
  initStorage();
  // Ajout d'un écouteur d'événements sur le touches alpha du clavier pour jouer
  document.addEventListener("keypress", (e) => {
    if ("abcdefghijklmnopqrstuvwxyz".includes(e.key)) {
      // console.debug(`Touche ${e.key.toUpperCase()} pressée`);
      document.querySelector(`#id-${e.key.toUpperCase()}`).click();
    }
  });
  // initialise le jeu
  initPartie();
};
